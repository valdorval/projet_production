-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 29 juin 2020 à 20:59
-- Version du serveur :  8.0.18
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projet_production`
--

-- --------------------------------------------------------

--
-- Structure de la table `blog`
--

DROP TABLE IF EXISTS `blog`;
CREATE TABLE IF NOT EXISTS `blog` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_profil` int(10) UNSIGNED NOT NULL,
  `title` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `article` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_id_profil` (`id_profil`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `blog`
--

INSERT INTO `blog` (`id`, `id_profil`, `title`, `article`) VALUES
(22, 1, 'Découvrez le tout nouveau Sub-discombobulateur Atomique', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec facilisis arcu non semper tincidunt. Pellentesque eget molestie elit. Nam tortor arcu, pellentesque gravida eros ut, hendrerit hendrerit erat. Proin euismod placerat sem vitae pellentesque. Duis consequat porta suscipit. Nam eu eros fringilla, luctus dolor ac, auctor nisi. Nam vitae erat ac felis eleifend egestas. Ut molestie tortor in odio hendrerit, in rutrum neque posuere. Praesent sagittis, est eu egestas suscipit, odio velit suscipit turpis, non molestie odio dolor in neque. Donec malesuada, nibh vitae aliquet gravida, ante tortor consequat lorem, in facilisis orci dolor vitae massa. Curabitur egestas risus sem, vitae congue massa venenatis eget. Nulla malesuada risus vitae varius consequat. Sed ut lacus malesuada, elementum orci non, tincidunt libero.\r\n\r\nPhasellus eget odio eget tellus venenatis eleifend eu quis nulla. Vivamus vel dignissim erat. Fusce vulputate ultrices placerat. Duis id pharetra est. In finibus eget nulla vitae gravida. Fusce quis turpis metus. Morbi ac tellus scelerisque, lobortis nibh ut, dictum diam. Sed posuere neque at diam porta, et facilisis velit condimentum. Donec rutrum ultricies tortor sit amet vehicula. Aliquam sit amet tortor ut ex posuere condimentum. Ut urna eros, ornare ut lacinia quis, malesuada ut sapien. Praesent mattis, odio non pulvinar sagittis, nisi lectus ornare enim, vel maximus ipsum quam id risus. Cras sit amet nulla ullamcorper nibh porta porta.\r\n\r\nCras felis elit, gravida ut tellus sit amet, tincidunt sodales nulla. Mauris et tellus eget purus pharetra pretium vel et felis. Nam pellentesque, ante ut tincidunt sagittis, lectus dui maximus purus, a faucibus ante metus non mi. Maecenas feugiat, eros vitae pulvinar vestibulum, mi turpis faucibus arcu, ac suscipit tellus ipsum ac ante. Curabitur et felis id elit volutpat rutrum. Ut vestibulum et odio quis lobortis. Nam commodo pretium risus non bibendum. Phasellus mauris mauris, lacinia quis sollicitudin at, eleifend in lorem.\r\n\r\nInteger pharetra erat scelerisque purus ornare porta. Etiam maximus ex eget urna dapibus bibendum. Praesent faucibus nulla turpis, eu porta mi porta in. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in dignissim ex. Cras facilisis risus dolor, ut mattis purus posuere sed. Sed sed nulla in lectus egestas accumsan nec sit amet risus. Morbi suscipit enim ligula, et viverra elit luctus ut. Curabitur efficitur et arcu non facilisis. Nam et faucibus urna.\r\n\r\nSuspendisse non volutpat purus, nec facilisis odio. Vivamus id porttitor est. Duis non efficitur ipsum. Cras ultrices neque fringilla tristique posuere. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sed lorem non diam vehicula lobortis. Sed tristique velit non fermentum scelerisque. Suspendisse vel auctor nisl. Quisque velit lectus, tempor vitae sagittis porta, semper id mauris. Maecenas rhoncus sit amet neque et feugiat. Aenean risus leo, bibendum nec consectetur et, sagittis iaculis augue. Mauris faucibus diam eget ex fermentum, non dignissim neque porttitor. Suspendisse aliquam iaculis lacinia. Vestibulum quis auctor lectus. Maecenas eu metus nec magna ullamcorper interdum et eget urna. Suspendisse at risus non tortor dapibus bibendum.\r\n\r\nVivamus vitae hendrerit justo. Mauris bibendum massa eget neque blandit imperdiet. Phasellus fringilla augue in ligula pretium congue. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed sapien tortor, auctor at vulputate in, laoreet eu ligula. Quisque tincidunt est vel lacus porta, fermentum cursus mauris pellentesque. In posuere eros quis sem tincidunt, a semper orci bibendum. In non ex nec nulla dictum elementum. Nam euismod leo a odio ultrices feugiat. Praesent ut tempor velit, in mattis lacus. Curabitur nec laoreet dolor. Proin a dignissim est.\r\n\r\nNunc sodales massa egestas, sodales ipsum ut, pellentesque quam. Nulla facilisi. Phasellus ut nisi interdum nulla mattis lacinia. Sed interdum laoreet mi, vehicula vehicula velit ultricies vel. Ut ligula nibh, finibus non pellentesque vitae, iaculis in quam. Sed sollicitudin semper turpis quis eleifend. Vestibulum a vulputate tellus. Mauris dignissim ante sed lacus congue mollis. Donec vulputate vulputate pretium. Integer dignissim lobortis scelerisque. Maecenas vitae tempor odio. Etiam mauris velit, facilisis nec suscipit facilisis, pulvinar nec magna. Morbi feugiat lacus non erat efficitur, non porttitor nisl consequat.\r\n\r\nMorbi ac gravida quam. Vivamus pretium risus non fermentum congue. In pellentesque tincidunt lectus non rutrum. Nam posuere rutrum nibh. Curabitur feugiat purus mi, in pulvinar velit rutrum sed. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Ut ut velit ac magna sodales vulputate. Phasellus eu justo sem. Sed justo augue, facilisis pellentesque tincidunt ac, blandit ac ante. Aliquam ullamcorper scelerisque elit, a finibus tortor pretium quis. Suspendisse est nunc, malesuada et odio sed, lobortis ultrices leo. Sed enim eros, blandit quis nulla eget, pharetra bibendum nunc. Sed lacinia nisi lectus, eget ultricies ante sollicitudin vel. Curabitur et bibendum tellus. Suspendisse scelerisque, ipsum sit amet pharetra aliquam, libero ligula dapibus libero, a tempus magna urna nec nisl. Nullam faucibus viverra fermentum.'),
(23, 1, 'L\'été arrive... La nouveauté aussi !', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque elementum ornare diam pharetra mattis. Duis vehicula dolor non odio sagittis, ut euismod velit euismod. Quisque nulla augue, lacinia at tempus in, consectetur in magna. Sed luctus tincidunt commodo. Donec quis vulputate odio, sit amet lobortis ex. In eget nulla sit amet ante malesuada congue. Integer consequat ante sit amet tellus posuere finibus. Fusce aliquam, quam a tristique convallis, dui ex vulputate nisi, in venenatis leo enim nec lacus. Vestibulum nec aliquam urna, et faucibus neque. Aenean in fringilla velit. Donec luctus imperdiet sapien, eget commodo ex.\r\n\r\nAenean eu mauris hendrerit, sodales ante quis, rhoncus risus. Vivamus vel metus sollicitudin, condimentum sem et, sollicitudin dui. Nam euismod, tortor vel porttitor auctor, dolor quam placerat lectus, ac dignissim erat ligula eget massa. Sed tempor metus dolor. Donec at tempor mauris. Ut in elementum est. Praesent dignissim maximus rutrum. Ut vulputate lectus purus, ut hendrerit justo accumsan imperdiet. Fusce facilisis facilisis eros ac accumsan.\r\n\r\nUt vel feugiat lorem. Nulla rhoncus tristique eros, vitae vestibulum sem ullamcorper id. Pellentesque tincidunt quis massa ac auctor. Cras vitae quam id sapien porta tincidunt. Suspendisse nec tincidunt massa. Vivamus mollis aliquam tellus eget consequat. Nam neque felis, dapibus at vehicula vel, malesuada eget massa. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris non nibh velit. Pellentesque accumsan lorem eget arcu accumsan, sit amet ornare orci pharetra. Fusce dui turpis, ullamcorper id lacus non, fermentum facilisis quam. Ut tincidunt justo sit amet sem rhoncus iaculis. Proin sed aliquet magna. Duis vehicula orci eu euismod fermentum.\r\n\r\nSed tincidunt posuere ex. Ut luctus nec nisi id pharetra. Ut viverra justo tortor, eget laoreet purus ornare eget. Mauris in sem facilisis, efficitur sem in, aliquam sem. Phasellus gravida dolor tellus, eu gravida neque commodo vitae. Donec malesuada pretium ante ac varius. Duis massa mi, varius a imperdiet viverra, lacinia ac arcu. Duis fermentum pellentesque lectus, in aliquet ex interdum ac. Etiam iaculis iaculis sem. Phasellus tincidunt ante nec leo sollicitudin dignissim. Maecenas gravida diam dignissim orci ornare laoreet. Integer eu pulvinar purus, sed semper lorem. Sed aliquam ac urna eu cursus. Pellentesque lobortis et orci nec vulputate. Maecenas accumsan libero in tempus accumsan. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.\r\n\r\nIn euismod, turpis consequat blandit ornare, magna arcu maximus felis, quis malesuada lorem sapien ut tellus. Suspendisse vitae est sapien. In hac habitasse platea dictumst. Ut efficitur augue eros, at pulvinar nisi porta sed. Nulla dignissim interdum magna ut sollicitudin. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam non convallis lectus. Vestibulum semper, diam id accumsan efficitur, magna urna bibendum est, quis bibendum dui urna nec massa. Phasellus vitae magna mollis, ultrices metus non, tempus urna. Morbi ac finibus erat. Donec venenatis a odio non vestibulum. Praesent aliquam at tortor non egestas. Mauris mattis auctor dolor, nec semper dolor hendrerit eu.\r\n\r\nOrci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur et scelerisque quam, et ultricies lacus. In hac habitasse platea dictumst. Aenean id imperdiet est. Nulla vel risus auctor, porttitor enim a, iaculis neque. Nulla eget urna vel orci faucibus euismod vel fermentum sem. Sed sodales turpis ac consectetur maximus. Fusce id tellus euismod nisl consequat eleifend. Pellentesque scelerisque dui et purus maximus, ut laoreet velit pulvinar. Ut congue augue nec metus feugiat, nec porttitor dui aliquet.\r\n\r\nVestibulum posuere scelerisque quam in vulputate. Quisque tempus sapien at ex viverra, tincidunt ornare justo maximus. Aenean bibendum risus nulla, nec molestie ex pulvinar at. Donec consequat nisl vitae nulla gravida auctor. Donec convallis risus nec turpis placerat semper. Integer vel sollicitudin augue, vitae porta ipsum. Vivamus varius dignissim enim, ut egestas enim finibus vel. Aliquam fringilla in dolor ut eleifend. Aenean purus nibh, vehicula nec tellus eu, consectetur mollis est. Nullam sed semper enim, id tincidunt arcu. Mauris at massa iaculis, malesuada massa nec, laoreet metus. Sed in dictum eros. Morbi tincidunt felis est, nec ornare ex dignissim et. Ut blandit nec leo at efficitur. Curabitur dapibus tortor nulla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.\r\n\r\nMaecenas non risus pulvinar, posuere mauris ut, mattis dolor. Maecenas congue in dui at malesuada. Proin quis nisi dignissim justo feugiat ullamcorper vitae eu lectus. Mauris a ultricies risus. Maecenas quis sodales lectus. Quisque volutpat aliquet nunc in volutpat. Morbi scelerisque lectus in mi condimentum sodales. Etiam lacinia, metus non mattis rutrum, felis augue fermentum urna, at posuere libero quam sit amet augue. Aliquam dictum rutrum est rutrum fringilla. Maecenas at velit ut justo pulvinar venenatis id vitae nunc. Maecenas nec tempor libero, nec rutrum ipsum. Duis finibus turpis sed convallis feugiat. Cras est erat, scelerisque nec varius in, euismod et purus. Nam sed risus vitae nulla hendrerit tempor non vitae purus. Curabitur vitae lacus lacinia, facilisis eros eget, semper nulla.\r\n\r\nNunc suscipit augue nunc, quis tempus dui imperdiet sit amet. Nullam consectetur enim nunc, id ultricies orci tincidunt ac. In sit amet euismod risus. Suspendisse pretium convallis nisl, gravida commodo massa varius a. Aenean a massa sed magna congue iaculis ut sit amet nunc. Suspendisse vel nunc vel lorem placerat dictum. Mauris ipsum sapien, iaculis id consequat a, tincidunt at nibh.\r\n\r\nNulla in leo ut orci luctus pretium. Etiam nec tellus dui. Suspendisse sodales quam massa, nec fermentum nibh mollis vel. Mauris sodales ante finibus, euismod neque in, pulvinar metus. Nullam non velit sed ipsum varius elementum. Sed rutrum sagittis nibh vel egestas. Vivamus et tortor eget quam vestibulum scelerisque. Nunc erat orci, ullamcorper pretium sollicitudin et, euismod sed orci. Aenean sed ultrices urna. Aliquam iaculis eu erat vitae mollis. Nunc in porttitor felis. Nullam at magna elit. Etiam hendrerit diam ac elit sagittis, vel mollis risus aliquet. Pellentesque pellentesque suscipit pretium.\r\n\r\nMaecenas metus odio, ultricies ac ante ac, tempus blandit augue. Mauris suscipit id mi vehicula pretium. Morbi ultricies aliquam consequat. Praesent tincidunt lacus ac magna gravida posuere. Donec sit amet dui et lacus auctor laoreet. Phasellus dapibus felis in elit porttitor, quis fringilla quam imperdiet. Praesent a neque blandit, consectetur risus id, interdum mauris. Ut eget erat a elit mattis vestibulum id accumsan mi. Nam sapien est, blandit a odio commodo, blandit dignissim eros. Sed nec dui neque. Sed vel quam vel ipsum iaculis mollis feugiat non ipsum. Aenean a efficitur tortor. Aliquam consectetur justo leo, eu porta nisl vehicula pellentesque. Pellentesque luctus lorem enim, nec cursus nisl rutrum ut. Mauris at scelerisque neque.'),
(26, 1, 'Bientôt... Soyez à l\'affût! ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur tincidunt lacus et leo hendrerit semper. Nunc luctus convallis turpis, quis congue nunc venenatis vel. In mollis ante sed est finibus, ut commodo diam imperdiet. Fusce eget sapien dictum, tempus dui ac, pretium orci. Aenean euismod ante nisi, eget commodo dui ullamcorper quis. Vestibulum bibendum ut est quis volutpat. Sed ultrices tincidunt mi, ac tempus felis tempus vitae. Nulla pharetra purus sit amet turpis tincidunt tristique. Curabitur eget iaculis est. Morbi at tortor tincidunt, tincidunt ligula et, vestibulum leo. Sed velit massa, accumsan egestas mattis a, tincidunt vitae risus. Aenean tincidunt elit eu metus imperdiet euismod. Cras ullamcorper, mauris in blandit dictum, urna risus tempor turpis, vel hendrerit dolor dolor at lacus. Mauris vel ante nec enim placerat tincidunt.\r\n\r\nSed ipsum nulla, faucibus at tempus quis, sodales vel neque. Nam odio eros, pharetra quis pellentesque quis, dictum quis libero. Fusce quis lectus ac neque ultrices consequat in ut neque. Donec mollis, lacus vel facilisis blandit, erat ipsum finibus orci, eu vestibulum elit ante sit amet turpis. Vestibulum eu egestas enim. Nam efficitur scelerisque pharetra. Quisque a tempor nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.\r\n\r\nSed non euismod enim. Vestibulum eu justo mattis, iaculis lorem quis, pretium urna. Praesent lacinia metus sit amet nisi pulvinar, vitae porttitor metus maximus. In hac habitasse platea dictumst. Vivamus dapibus ex sem, eget mattis mi sagittis vulputate. Cras sit amet risus volutpat orci pulvinar pharetra. Vestibulum consequat hendrerit iaculis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed rhoncus aliquam orci, sed aliquam neque aliquam vel. Donec ut malesuada magna. Sed vitae sapien ante. Curabitur scelerisque nibh sagittis viverra elementum. Phasellus tempor scelerisque nibh a mollis.\r\n\r\nMorbi at porta magna. In condimentum est lorem, sit amet semper sem rutrum a. Maecenas condimentum tincidunt faucibus. Fusce nec dolor at magna commodo iaculis eget lobortis orci. Aenean volutpat pulvinar aliquam. Donec facilisis ligula nec leo commodo scelerisque. In dignissim mollis enim, vitae porttitor odio. Morbi et tincidunt diam. Aliquam a dolor euismod purus iaculis rhoncus.');

-- --------------------------------------------------------

--
-- Structure de la table `newsletter`
--

DROP TABLE IF EXISTS `newsletter`;
CREATE TABLE IF NOT EXISTS `newsletter` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `base` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `pas_base` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `luxe` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `news` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `newsletter`
--

INSERT INTO `newsletter` (`id`, `email`, `base`, `pas_base`, `luxe`, `news`) VALUES
(1, 'val@val.com', 1, 1, 0, 0),
(3, 'val@val.com', 1, 1, 0, 0),
(5, 'kylia@ya.com', 0, 1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `version` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `dispo` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `product`
--

INSERT INTO `product` (`id`, `version`, `dispo`) VALUES
(1, 'Version de base', '2047-08-12'),
(2, 'Version de pas base', '2047-08-19'),
(3, 'Version de gros gros luxe', '2047-08-28');

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
CREATE TABLE IF NOT EXISTS `reservation` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `pwd` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `is_admin` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_product` (`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=995 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `reservation`
--

INSERT INTO `reservation` (`id`, `name`, `email`, `pwd`, `id_product`, `is_admin`) VALUES
(1, 'Valerie Dorval', 'val@val.com', '$2y$10$MV.iZ7KkaUOCA28vhOXlDONQ/LjByqnXcOXT27mGqcdr0WmZ4OovC', 2, 1),
(993, 'Kylia Willer', 'kylia@ya.com', '$2y$10$zG4n6SLYYS2rw9cCF.pK6.ZtImTWQ2O9eRMRuQSXji7/vDK1tCbFq', 3, 0);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `blog`
--
ALTER TABLE `blog`
  ADD CONSTRAINT `fk_id_profil` FOREIGN KEY (`id_profil`) REFERENCES `reservation` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `fk_product` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
