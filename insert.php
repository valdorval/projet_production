<?php
require 'include/element/header.php';

if ($errors) {
        foreach ($errors as $error) {
                if ($error === '') {
                        $connexion = true;
                } else {
                        $connexion = false;
                }
        }
}

if ($connexion) {
        try {
                $conn = connect();
                $req = $conn->prepare('INSERT INTO blog (`id_profil`, `title`, `article`)
                VALUES (?, ?, ?)');
                $req->execute([valid_input($_SESSION['auth']['id']), valid_input($_POST['title']),  valid_input($_POST['content'])]);
                $_SESSION['flash'] = 'Le nouvel article "' .  $_POST['title'] . '" a bien été ajouté!';
                header('Location: admin.php');
        } catch (PDOException $e) {
                echo 'Erreur : ' . $e->getMessage();
        }
        $connect = null;
        exit();
}

if ($_SESSION['auth']['is_admin'] === 1) : ?>

        <section class="insert">
                <h2>Ajouter un nouvel article</h2>

                <div class="insert-form">
                        <form action="" method="POST">
                                <div>
                                        <span class="reservation_error"><?php echo $errors['title'] ?></span>
                                        <input type="text" name="title" id="title" class="input large" placeholder="Titre de l'article" value="<?= form_values('title'); ?>">
                                </div>
                                <div>
                                        <span class="reservation_error"><?php echo $errors['content'] ?></span>
                                        <textarea name="content" id="" class="input large" placeholder="Contenu de l'article">
                                        <?= form_values('content'); ?>
                                        </textarea>
                                </div>
                                <button type="submit" name="insert_submit" class="btn btn-warning">Envoyer</button>
                        </form>
                        <a href="admin.php" class="retour">Retour</a>
                </div>

        </section>

<?php else :
        header('Location: index.php');
        exit();
endif; ?>

</main>

<?php
require 'include/element/footer.php';
?>