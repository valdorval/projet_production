<?php
include 'include/element/header.php';
include 'reservation.php';
include 'newsletter.php';
?>

<body>

     <header class="header" id="header">

          <div class="header_nav">
               <h1 class="animate__animated animate__flipInX animate__slow">Sub-discombobulateur Atomique</h1>
          </div>

          <div class="navigation">
               <input type="checkbox" class="nav_checkbox" id="navi-toggle">
               <label for="navi-toggle" class="navi_btn">
                    <span class="navigation_icon">&nbsp;</span>
               </label>
               <div class="nav_background">&nbsp;</div>

               <nav class="navigation_nav">
                    <ul class="navigation_list">
                         <li class="navigation_item"><a href="#infolettre" class="navigation_link">Infolettre</a></li>
                         <li class="navigation_item"><a href="#product" class="navigation_link">Produits</a>
                         </li>
                         <li class="navigation_item"><a href="#blog" class="navigation_link">Blog</a></li>
                         <li class="navigation_item"><a href="#reservation" class="navigation_link">Réservation</a></li>
                         <?php if (isset($_SESSION['auth'])) : ?>
                              <a href="logout.php" class="btn btn-warning">Déconnexion</a>
                         <?php else : ?>
                              <button type="button" class="btn btn-warning" id="loginBtn">Se connecter</button>
                         <?php endif; ?>
                    </ul>
               </nav>
          </div>

          <div class="connect" id="connect">
               <div class="connect_form">
                    <div class="connect_bg">
                         <div class="connect_close"></div>
                         <h2>Connexion à votre compte</h2>
                         <span class="reservation_error" id="error_login"></span>
                         <form action="" method="POST" id="form_login">
                              <div class="nav_form_group">
                                   <input type="text" name="login_mail" id="login_mail" placeholder="Adresse courriel" class="nav_form_input">
                              </div>
                              <div class="nav_form_group">
                                   <input type="password" name="login_pwd" id="login_pwd" placeholder="Mot de passe" class="nav_form_input">
                              </div>

                              <button type="submit" name="login_submit" id="login_submit" class="btn btn-warning">Connexion</button>
                         </form>
                    </div>
               </div>
          </div>

          <section class="infolettre" id="infolettre">
               <div class="infolettre_main">
                    <?php
                    if (isset($_SESSION['flash']) && !empty($_SESSION['flash'])) {
                         echo '<div class="flash"><p>' . $_SESSION['flash'];
                         unset($_SESSION['flash']);
                         echo '</p></div>';
                    }
                    ?>
                    <div class="info_logo">
                         <img src="img/logo_v2.svg" alt="logo">
                    </div>
                    <div class="info_new">
                         <h2 class="info_title">Nouveau!</h2>
                         <div class="info_content">
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum vel
                                   urna et lobortis. Aliquam faucibus consequat malesuada. Donec sed pulvinar dolor. Ut
                                   vitae
                                   blandit quam. Maecenas libero turpis, commodo ac diam et, semper varius neque.</p>
                         </div>
                         <div class="text-center">
                              <button type="button" class="btn btn-dark info_btn" id="modal"><a href="#">Restez
                                        informé!</a></button>
                         </div>
                    </div>
               </div>

               <div class="infolettre_inscription">
                    <div class="infolettre_form">
                         <div class="infolettre_bg">
                              <div class="info_close"></div>
                              <form action="" method="POST">
                                   <div class="infolettre_header">
                                        <h2>Infolettre</h2>
                                        <p>Inscrivez-vous dès maintenant à notre infolettre pour tout savoir sur nos
                                             produits et ne rien manquer de notre actualité!</p>
                                   </div>
                                   <span class="reservation_error">
                                        <?php
                                        if (isset($errors_news) && !empty($errors_news)) {
                                             foreach ($errors_news as $error) {
                                                  echo '<span class="reservation_error">' . $error . '</span>';
                                             }
                                        }
                                        ?>

                                   </span>
                                   <div class="infolettre_group_form">
                                        <input type="email" name="email" placeholder="Adresse courriel" id="infolettre_email" class="infolettre_input" value="<?php form_values('email') ?>">
                                   </div>
                                   <span class="info_pref">Vos préférences</span>
                                   <div class="info_radio_group">
                                        <input type="checkbox" class="info_radio_input" id="info_base" name="base" value="1">
                                        <label for="info_base" class="info_radio_label">
                                             <span class="info_radio_btn"></span>
                                             Base
                                        </label>
                                   </div>
                                   <div class="info_radio_group">
                                        <input type="checkbox" class="info_radio_input" id="info_pas_base" name="pas_base" value="1">
                                        <label for="info_pas_base" class="info_radio_label">
                                             <span class="info_radio_btn"></span>
                                             Pas base
                                        </label>
                                   </div>
                                   <div class="info_radio_group">
                                        <input type="checkbox" class="info_radio_input" id="info_luxe" name="luxe" value="1">
                                        <label for="info_luxe" class="info_radio_label">
                                             <span class="info_radio_btn"></span>
                                             Gros gros luxe
                                        </label>
                                   </div>
                                   <div class="info_radio_group">
                                        <input type="checkbox" class="info_radio_input" id="info_actu" name="actu" value="1">
                                        <label for="info_actu" class="info_radio_label">
                                             <span class="info_radio_btn"></span>
                                             Actualités
                                        </label>
                                   </div>
                                   <button class="btn btn-warning" type="submit" name="newsletter_submit">S'inscrire</button>
                              </form>
                         </div>

                    </div>
               </div>

          </section>
     </header>

     <main class="main">

          <section class="product" id="product">
               <div id="my-slider" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">

                         <div class="carousel-item active">
                              <div class="flex carousel_content">
                                   <div class="carousel_detail">
                                        <h2 class="carousel_title">Version de base</h2>
                                        <div class="carousel_p">
                                             <p>Valable pour deux utilisations.</p>
                                             <p>Elle attire jusqu’à 50% des molécules atomiques 15 pieds autour de son
                                                  utilisateur.</p>
                                             <p>Pour plus de sécurité, elle est impossible à modifier. Si la boite est
                                                  ouverte, elle explose</p>
                                             <p>Elle est de couleur blanche</p>
                                        </div>
                                   </div>
                                   <div class="carousel_img">
                                        <h3 class="carousel_img_title">1 250,99$</h3>
                                        <img src="img/version1.jpg" alt="version de base">
                                        <button class="btn btn-secondary carousel_btn"><a href="#reservation" class="scroll">Réservez
                                                  maintenant!</a></button>
                                   </div>
                              </div>
                         </div>

                         <div class="carousel-item">
                              <div class="flex carousel_content">
                                   <div class="carousel_detail">
                                        <h2 class="carousel_title">Version de pas base</h2>
                                        <div class="carousel_p">
                                             <p>Valable pour deux utilisations.</p>
                                             <p>Elle attire jusqu’à 25 % des molécules atomiques 10 pied autour de son
                                                  utilisateur.</p>
                                             <p>Pour plus de sécurité,elle est trempée dans un plastique qui la rend
                                                  monobloc et impénétrable.</p>
                                             <p>Elle est de couleur pastel</p>
                                        </div>
                                   </div>
                                   <div class="carousel_img">
                                        <h3 class="carousel_img_title">3 050,74$</h3>
                                        <img src="img/version2.jpg" alt="version de pase base">
                                        <button class="btn btn-secondary carousel_btn"> <a href="#reservation" class="scroll">Réservez
                                                  maintenant!</a> </button>
                                   </div>
                              </div>
                         </div>

                         <div class="carousel-item">
                              <div class="flex carousel_content">
                                   <div class="carousel_detail">
                                        <h2 class="carousel_title">Version de gros gros luxe</h2>
                                        <div class="carousel_p">
                                             <p>Valable pour deux utilisations.</p>
                                             <p>N'attire aucune molécule nocive</p>
                                             <p>Pour plus de sécurité, elle est trempée dans un plastique qui la rend
                                                  monobloc et impénétrable. Elle est aussi recouverte d'une couche
                                                  d'acier pour encore plus d'impénétrabilité</p>
                                             <p>De plus, une couverture de fonte permet de ne pas la perdre car au poid
                                                  qu'il a vous ne pouvez pas l'oublier.</p>
                                             <p>Deux choix de couleurs: rouge ou brun vif</p>
                                        </div>
                                   </div>
                                   <div class="carousel_img">
                                        <h3 class="carousel_img_title">10 287,23$</h3>
                                        <img src="img/version3.jpg" alt="version de base">
                                        <button class="btn btn-secondary carousel_btn"><a href="#reservation" class="scroll">Réservez
                                                  maintenant!</a></button>
                                   </div>
                              </div>
                         </div>

                         <a href="#my-slider" class="carousel-control-prev" role="button" data-slide="prev">
                              <span class="fa fa-arrow-circle-left fa-1x"></span>
                         </a>

                         <a href="#my-slider" class="carousel-control-next" role="button" data-slide="next">
                              <span class="fa fa-arrow-circle-right fa-1x"></span>
                         </a>

                    </div>
               </div>
          </section>

          <section class="blog" id="blog">
               <?php
               $article = connect_blog();
               foreach ($article as $detail) {
                    echo '<div class="blog_article" id="article' . $detail['id'] . '">';
                    echo '<div class="blog_close b_close"></div>';
                    echo '<div class="blog_article_bg">';
                    echo '<h2>' . $detail['title'] . '</h2>';
                    echo '<h4>' .  $detail['name'] . '</h4>';
                    echo '<p>' . nl2br($detail['article']) . '</p>';
                    echo '<button class="btn btn-warning b_close" type="button">Fermer</button>';
                    echo '</div></div>';
               } ?>

               <h3 class="blog_title">Quoi de neuf?</h3>

               <?php
               $article = connect_blog();
               foreach ($article as $detail) {
                    echo '<div class="blog_bg flex">';
                    echo '<div class="blog_author">';
                    echo '<div class="blog_author_content">';
                    echo '<h4>' . $detail['name'] . '</h4>';
                    echo '</div></div>';
                    echo '<div class="blog_content">';
                    echo '<h4 class="blog_content_title">' . $detail['title'] . '</h4>';
                    echo '<p class="blog_content_p">' . nl2br(substr($detail['article'], 0, 550)) . '</p>';
                    echo '<button type="button" class="btn btn-warning blog_btn" data-info="article' . $detail['id']  . '">Lire la
                    suite</button>';
                    echo '</div></div>';
               }
               $article = null;
               ?>

          </section>

          <?php if (isset($_SESSION['auth'])) : ?>
               <?php
               $connexion = connect_by_id($_SESSION['auth']['id']);
               $user =  $connexion->fetch();
               ?>
               <section class="profil">
                    <div class="profil_bg">
                         <h2 class="profil_welcome">Bienvenue <?php echo $user['name'] ?>!</h2>
                         <h3 class="profil_title">Détails de votre réservation</h3>
                         <div class="profil_content flex">
                              <div class="profil_info">
                                   <p>
                                        <span>Produit commandé:</span>
                                        Sub-Discombobulateur Atomique <br>
                                        <?php echo $user['version'] ?>
                                   </p>
                                   <p>
                                        <span>Disponibilité</span>
                                        <?php echo $user['dispo'] ?>
                                   </p>
                              </div>
                              <div class="profil_product">
                                   <img src="img/version<?php echo $user['id_product'] ?>.jpg" alt="version de luxe">
                              </div>
                         </div>
                         <p class="profil_important">*IMPORTANT! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ornare libero sapien. Fusce vitae consectetur ex, eget scelerisque urna. Nunc venenatis nisl vitae nulla sodales, pretium malesuada sapien mattis. Donec sagittis porttitor nunc sit amet volutpat. Cras eget vestibulum enim. Nullam vestibulum a orci quis varius. Aliquam congue blandit sem eu dictum. Nulla facilisi. </p>
                    </div>
               </section>
          <?php else : ?>
               <section class="reservation" id="reservation">
                    <div class="reservation_form_bg">
                         <div class="reservation_form_img"></div>
                         <div class="reservation_form">

                              <form action="index.php#reservation" method="POST" class="form">
                                   <h3 class="reservation_title">Réservez maintenant votre
                                        Sub-Discombobulateur atomique!</h3>

                                   <?php
                                   if (isset($errors) && !empty($errors)) {
                                        foreach ($errors as $error) {
                                             echo '<span class="reservation_error">' . $error . '</span>';
                                        }
                                   }

                                   ?>

                                   <div class="res_radio_group">
                                        <input type="radio" class="res_radio_input" id="base" name="reservation_choice" value="1">
                                        <label for="base" class="res_radio_label">
                                             <span class="res_radio_btn"></span>
                                             Base
                                        </label>
                                   </div>
                                   <div class="res_radio_group">
                                        <input type="radio" class="res_radio_input" id="pas_base" name="reservation_choice" value="2">
                                        <label for="pas_base" class="res_radio_label">
                                             <span class="res_radio_btn"></span>
                                             Pas base
                                        </label>
                                   </div>
                                   <div class="res_radio_group">
                                        <input type="radio" class="res_radio_input" id="luxe" name="reservation_choice" value="3">
                                        <label for="luxe" class="res_radio_label">
                                             <span class="res_radio_btn"></span>
                                             Gros gros luxe
                                        </label>
                                   </div>
                                   <div class="res_form_group">
                                        <input type="text" name="reservation_name" id="reservation_name" class="res_input" placeholder="Nom complet" value="<?php form_values('reservation_name') ?>">
                                        <label for="reservation_name" class="res_label"></label>
                                   </div>
                                   <div class="res_form_group">
                                        <input type="email" name="reservation_mail" id="reservation_mail" class="res_input" placeholder="Adresse courriel" value="<?php form_values('reservation_mail') ?>">
                                        <label for="reservation_mail" class="res_label"></label>
                                   </div>
                                   <div class="res_form_group">
                                        <input type="password" name="reservation_pwd" id="reservation_pwd" class="res_input" placeholder="Mot de passe">
                                        <label for="reservation_pwd" class="res_label"></label>
                                   </div>
                                   <div class="res_form_group">
                                        <input type="password" name="reservation_confirm_pwd" id="reservation_confirm_pwd" class="res_input" placeholder="Confirmation du mot de passe">
                                        <label for="reservation_confirm_id" class="res_label"></label>
                                   </div>
                                   <button type="submit" name="reservation_submit" class="btn btn-warning">Envoyer</button>
                              </form>

                         </div>
                    </div>

               </section>
          <?php endif; ?>

     </main>

     <?php
     include 'include/element/footer.php';
     ?>