<?php

require 'include/element/header.php';

$conn = connect_blog_by_id($_GET['id']);
$nom = $conn->fetch();

if (!empty($_POST)) {
        $connect = connect();
        $id = $_GET['id'];
        $req = $connect->prepare('DELETE FROM blog WHERE id = ?');
        $req->execute([$id]);
        $_SESSION['flash'] = 'L\'article "' . $nom['title'] . '" a été effacé avec succès!';
        header('Location: admin.php');
        $connect = null;
}

?>

<section class="delete">
        <h2>Suppression de l'article: <?php echo $nom['title']; ?></h2>

        <div class="delete-content">
                <div class="delete-form">
                        <form action="" method="POST">
                                <p>Êtes-vous sur de vouloir effacer cet article?</p>
                                <div>
                                        <button type="submit" name="delete-submit" class="btn btn-secondary">Oui</button>
                                        <button type="button" class="btn btn-danger"><a href="admin.php">Non</a></button>
                                </div>

                        </form>

                </div>
        </div>

</section>



</main>

<?php
require 'include/element/footer.php';
?>