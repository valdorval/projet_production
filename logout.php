<?php
session_start();
unset($_SESSION['auth']);
session_destroy();
$_SESSION['flash'] = 'Vous êtes maintenant déconnecté!';
header('Location: index.php');
