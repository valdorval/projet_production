$(function () {

        function fadeInItem(item, modify) {
                item.click(function () {
                        modify.fadeIn();
                })
        }

        function closeItem(item, modify) {
                item.click(function () {
                        modify.fadeOut();
                })
        }

        fadeInItem($('#modal'), $('.infolettre_inscription'));
        closeItem($('.info_close'), $('.infolettre_inscription'));
        fadeInItem($('#loginBtn'), $('.connect'));
        closeItem($('.connect_close'), $('.connect'));
        closeItem($('.b_close'), $('.blog_article'));

        /* ------------------------ tentative login ajax ------------------------------ */

        // $(document).on('click', '#login_submit', function (e) {
        //         var data = $('#form_login').serialize();
        //         e.preventDefault();
        //         $.ajax({
        //                 method: 'POST',
        //                 url: '/login.php',
        //                 data: data,
        //                 success: function (response) {
        //                         console.log(response);
        //                         if (data == 1) {
        //                                 console.log('sucess');
        //                                 $('#connect').fadeOut();
        //                                 window.location = "index.php";
        //                         } else {
        //                                 console.log('error');
        //                                 $('#error_login').text('L\'adresse courriel ou le mot de passe est incorrect');
        //                         }
        //                 }
        //         })
        // })

        // $('#login_submit').click(function (e) {
        //         e.preventDefault();

        //         $.post(
        //                 'login.php',
        //                 {
        //                         email: $('#login_mail').val(),
        //                         password: $('#login_pwd').val()
        //                 },
        //                 function (data) {
        //                         if (data === 'Login') {
        //                                 $('#connect').fadeOut();
        //                                 document.location.reload();
        //                         } else {
        //                                 $('#error_login').html('L\'adresse courriel ou le mot de passe est incorrect');

        //                         }
        //                 },
        //                 'text'
        //         );

        // })

        $('.scroll').click(function (e) {
                e.preventDefault();

                var target = $($(this).attr("href"));

                $('html, body').animate({
                        'scrollTop': target.offset().top
                }, 500, 'swing');
        });

        $('.blog_btn').click(function () {
                var myId = $(this).attr('data-info');
                $('#' + $(this).data('info')).fadeIn();
        })

});