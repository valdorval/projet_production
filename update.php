<?php
include 'include/element/header.php';
$id_blog = $_GET['id'];

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['edit_submit'])) {
        $db = connect();
        try {
                $data =  $db->prepare('UPDATE blog SET title = ?, article = ? WHERE id = ?');
                $data->execute([$_POST['title'], $_POST['content'], $id_blog]);
                $_SESSION['flash'] = 'L\'article "' .  $_POST['title'] . '" a été modifié avec succès!';
                header('Location: admin.php');
        } catch (PDOException $e) {
                echo 'Erreur: ' . $e->getMessage();
        }
        $db = null;
        exit();
}

if ($_SESSION['auth']['is_admin'] === 1) : ?>

        <section class="update">
                <h2>Ajouter un nouvel article</h2>

                <div class="update-form">
                        <form action="" method="POST">
                                <div>
                                        <?php
                                        $conn = connect_blog_by_id($id_blog);
                                        $article = $conn->fetch();
                                        ?>

                                        <input type="text" name="title" id="u_title" class="input large" placeholder="Titre de l'article" value="<?php echo $article['title']; ?>">
                                        <span class="reservation_error"></span>
                                </div>
                                <div>
                                        <textarea name="content" id="u_content" class="input large" placeholder="Contenu de l'article"><?php echo $article['article']; ?></textarea>
                                        <span class="reservation_error"></span>
                                </div>
                                <button type="submit" name="edit_submit" class="btn btn-warning">Enregistrer modification</button>
                        </form>
                        <a href="admin.php" class="retour">Retour</a>
                </div>

        </section>

<?php else :
        header('Location: index.php');
        exit();
endif;
require 'include/element/footer.php';
?>