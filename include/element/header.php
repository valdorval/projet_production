<?php
include 'include/function/session.php';
include 'include/function/function.php';
include 'include/function/data/db.php';
include 'login.php';
?>


<!doctype html>
<html class="no-js" lang="fr">

<head>
        <meta charset="utf-8">
        <title>Nouveau - Boutique en ligne - Sub-Discombobulateur Atomique</title>
        <meta name="description" content="Nouvelle boutique en ligne - Sub-Discombobulateur Atomique">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="manifest" href="site.webmanifest">
        <link rel="apple-touch-icon" href="icon.png">
        <!-- Place favicon.ico in the root directory -->
        <link href="https://fonts.googleapis.com/css2?family=Capriola&family=Crimson+Text:wght@400;600;700&family=Jura:wght@400;500;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" />
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/style.css">

        <meta name="theme-color" content="#343a40">
</head>