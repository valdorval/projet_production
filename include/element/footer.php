<footer class="footer">
        <div class="footer_top flex">
                <h2>Simsonne Cire</h2>
                <div class="footer_social flex">
                        <a href="#"><img src="img/facebook.svg" alt="facebook"></a>
                        <a href="#"><img src="img/twitter.svg" alt="twitter"></a>
                        <a href="#"><img src="img/instagram.svg" alt="instagram"></a>
                </div>
        </div>
        <div class="footer_info">
                <a href="#">Information</a>
                <a href="#">Support</a>
                <a href="#">Contact</a>
        </div>
        <div class="footer_legal">
                <a href="#">Modalités d'utilisation</a>
                <a href="#">Vie privée</a>
        </div>
        <div class="footer_detail">
                <p>© Agence É 2020 - Valérie Dorval</p>
        </div>
</footer>


<script src="js/vendor/modernizr-3.8.0.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>

<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
<script>
        window.ga = function() {
                ga.q.push(arguments)
        };
        ga.q = [];
        ga.l = +new Date;
        ga('create', 'UA-XXXXX-Y', 'auto');
        ga('set', 'transport', 'beacon');
        ga('send', 'pageview')
</script>
<script src="https://www.google-analytics.com/analytics.js" async></script>
</body>

</html>