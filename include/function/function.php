<?php
$errors = null;
$connexion = null;
$errors_news = [];

/* ================ General ================= */

function valid_input($data)
{
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
}

function encrypt_pass($pass)
{
        $hash_pwd = password_hash($pass, PASSWORD_BCRYPT);
        return $hash_pwd;
}

/* ================ Connect ================= */

function connect()
{
        $serveur = "localhost"; //127.0.0.1
        $dbname = "projet_production";
        $user = "root";
        $password = "";
        $options = [
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_EMULATE_PREPARES => false,
        ];

        try {
                $pdo = new PDO("mysql:host=$serveur;dbname=$dbname", $user, $password, $options);
                return $pdo;
        } catch (PDOException $e) {
                throw new PDOException($e->getMessage(), (int) $e->getCode());
        }
        return $pdo;
}

function connect_blog()
{
        $conn = connect();
        try {
                $req = $conn->query('SELECT b.title, b.article, b.id, b.id_profil, r.name
                FROM blog AS b
                LEFT OUTER JOIN reservation AS r ON b.id_profil = r.id');
                return $req;
        } catch (PDOException $e) {
                echo 'Erreur: ' . $e->getMessage();
        }
        $conn = null;
        exit();
}

function connect_blog_by_id($id)
{
        $conn = connect();
        try {
                $requete = $conn->prepare("SELECT * FROM blog WHERE id = ?");
                $requete->execute([$id]);
                return $requete;
        } catch (PDOException $e) {
                echo 'Erreur : ' . $e->getMessage();
        }
        $conn = null;
        exit();
}

function connect_by_id($id)
{
        $conn = connect();
        try {
                $requete = $conn->prepare('SELECT r.name, r.id_product, p.version, p.dispo
                FROM reservation AS r
                LEFT OUTER JOIN product AS p ON r.id_product = p.id
                WHERE r.id = ?');
                $requete->execute([$id]);
                return $requete;
        } catch (PDOException $e) {
                echo 'Erreur : ' . $e->getMessage();
        }
        $conn = null;
        exit();
}

/* ================ Validate ================= */

function form_values($nom)
{
        echo (isset($_POST[$nom]) ? $_POST[$nom] : "");
}

function validate_email($courriel)
{
        return filter_var($courriel, FILTER_VALIDATE_EMAIL);
}

function validate_newsletter()
{
        $errors_news['email'] = (empty(valid_input($_POST['email'])) || !validate_email($_POST['email']) ? '- L\'adresse courriel n\'est pas valide' : '');

        if (empty($_POST['base']) && empty($_POST['pas_base']) && empty($_POST['luxe']) && empty($_POST['actu'])) {
                $errors_news['checkbox'] = '- Vous devez choisir au moins une version de l\'infolettre';
        } else {
                $errors_news['checkbox'] = '';
        }

        if (count($errors_news) >= 1) {
                return $errors_news;
        }
}

if ($_SERVER["REQUEST_METHOD"] == "POST"  && isset($_POST['newsletter_submit'])) {
        $form = validate_newsletter();
        if (count($form) > 1) {
                $errors_news = $form;
        }
        return $errors_news;
}

function validate_reservation()
{
        $errors = [];
        $errors['reservation_name'] = (empty(valid_input($_POST['reservation_name'])) ? '- Le nom ne peut pas être vide' : '');
        $errors['reservation_mail'] = (empty(valid_input($_POST['reservation_mail'])) || !validate_email($_POST['reservation_mail']) ? '- L\'adresse courriel n\'est pas valide' : '');
        $errors['reservation_choice'] = (empty($_POST['reservation_choice']) ? '- Veuillez choisir une version' : '');

        if (empty(valid_input($_POST['reservation_pwd']))) {
                $errors['reservation_pwd'] = '- Le mot de passe ne peut pas être vide';
        } elseif ($_POST['reservation_pwd'] != $_POST['reservation_confirm_pwd']) {
                $errors['reservation_pwd'] = '- Le mot de passe n\'est pas le même';
        } else {
                $errors['reservation_pwd'] = '';
        }

        if (count($errors) >= 1) {
                return $errors;
        }
}

if ($_SERVER["REQUEST_METHOD"] == "POST"  && isset($_POST['reservation_submit'])) {
        $form = validate_reservation();
        if (count($form) > 1) {
                $errors = $form;
        }
        return $errors;
}

function validate_blog()
{
        $errors = [];
        $errors['title'] = (empty(valid_input($_POST['title'])) ? 'Le titre ne peut pas être vide' : '');
        $errors['content'] = (empty(valid_input($_POST['content'])) ? 'Le contenu de l\'article ne peut pas être vide' : '');

        if (count($errors) >= 1) {
                return $errors;
        }
}

if ($_SERVER["REQUEST_METHOD"] == "POST"  && isset($_POST['insert_submit'])) {
        $form = validate_blog();
        if (count($form) > 1) {
                $errors = $form;
        }
        return $errors;
}
