<?php

$serveur = "localhost"; //127.0.0.1
$dbname = "projet_production";
$user = "root";
$password = "";
$options = [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_EMULATE_PREPARES => false,
];

try {
        $pdo = new PDO("mysql:host=$serveur;dbname=$dbname", $user, $password, $options);
        return $pdo;
} catch (PDOException $e) {
        throw new PDOException($e->getMessage(), (int) $e->getCode());
}
