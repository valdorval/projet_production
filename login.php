<?php
include 'include/function/data/db.php';

if (!empty($_POST['login_mail']) && !empty($_POST['login_pwd'])) {
        $email = $_POST['login_mail'];
        $pwd = $_POST['login_pwd'];

        try {
                $requete = $pdo->prepare('SELECT * FROM reservation WHERE email = ?');
                $requete->execute([$email]);
                $userInfo = $requete->fetch();

                if ($email === $userInfo['email']) {
                        if (password_verify($pwd, $userInfo['pwd'])) {
                                $result = 1;
                                $_SESSION['auth'] = $userInfo;
                                $_SESSION['flash'] = 'Connexion réussie';
                        } else {
                                $result = 0;
                        }
                        return $result;
                }
        } catch (PDOException $e) {
                echo 'Erreur : ' . $e->getMessage();
        }
}
