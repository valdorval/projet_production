<?php
include 'include/function/data/db.php';

$news_conn = null;
$base = 0;
$pas_base = 0;
$luxe = 0;
$actu = 0;

if ($errors_news) {
        foreach ($errors_news as $error) {
                if ($error === '') {
                        $news_conn = true;
                } else {
                        $news_conn = false;
                }
        }
}

if (isset($_POST['base']) && $_POST['base'] === '1') {
        $base = 1;
}

if (isset($_POST['pas_base']) && $_POST['pas_base'] === '1') {
        $pas_base = 1;
}

if (isset($_POST['luxe']) && $_POST['luxe'] === '1') {
        $luxe = 1;
}

if (isset($_POST['actu']) && $_POST['actu'] === '1') {
        $actu = 1;
}

if ($news_conn) {
        try {
                $req = $pdo->prepare('INSERT INTO newsletter (email, base, pas_base, luxe, news)
             VALUES (?, ?, ?, ?, ?)');
                $req->execute([$_POST['email'], $base, $pas_base, $luxe, $actu]);
                $_SESSION['flash'] = 'L\'inscription à l\'infolettre a bien été effectuée';
                header('Location: index.php');
        } catch (PDOException $e) {
                echo 'Erreur : ' . $e->getMessage();
        }
}
