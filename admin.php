<?php
include 'include/element/header.php';

if ($_SESSION['auth']['is_admin'] === 1) :
?>

        <section class="admin">
                <a href="index.php" class="retour">Retour au site</a>
                <header class="flex">
                        <h2>Liste des articles du blog</h2>
                        <a href="insert.php" class="retour"><button class="btn btn-warning"> Ajouter <span>+</span> </button></a>
                </header>

                <?php
                if (isset($_SESSION['flash']) && !empty($_SESSION['flash'])) {
                        echo '<div class="flash"><p>' . $_SESSION['flash'] . '</p></div>';
                }

                ?>

                <div class="admin-list">
                        <table>
                                <thead>
                                        <tr>
                                                <th>Titre de l'article</th>
                                                <th>Auteur</th>
                                                <th class="preview">Aperçu du texte</th>
                                                <th class="action">Action</th>
                                        </tr>
                                </thead>
                                <tbody>

                                        <?php
                                        $article = connect_blog();
                                        foreach ($article as $detail) {
                                                echo '<tr>';
                                                echo '<td>' . $detail['title'] . '</td>';
                                                echo '<td>' . $detail['name'] . '</td>';
                                                echo '<td>' . substr($detail['article'], 0, 300) . '</td>';
                                                echo '<td>';
                                                echo '<a href="update.php?id=' . $detail['id'] . '" class="button-table"><img src="img/001-pencil.png" alt="edition" /></a>';
                                                echo '<a href="delete.php?id=' . $detail['id'] . '" class="button-table"><img src="img/002-delete.png" alt="effacer"></a>';
                                                echo '</td>';
                                                echo '</tr>';
                                        }
                                        $article = null;
                                        ?>

                                </tbody>

                        </table>

                </div>

        </section>

<?php else :
        header('Location: index.php');
        exit();
endif; ?>

</main>




<?php
include 'include/element/footer.php';
?>