<?php
include 'include/function/data/db.php';

if ($errors) {
        foreach ($errors as $error) {
                if ($error === '') {
                        $connexion = true;
                } else {
                        $connexion = false;
                }
        }
}

if ($connexion) {
        try {
                $req = $pdo->prepare('INSERT INTO reservation (name, email, pwd, id_product)
             VALUES (?, ?, ?, ?)');
                $password = encrypt_pass($_POST['reservation_pwd']);
                $req->execute([$_POST['reservation_name'], $_POST['reservation_mail'], $password, $_POST['reservation_choice']]);
                $_SESSION['flash'] = 'La réservation a bien été effectuée. Vous pouvez maintenant vous connecter pour voir les détails.';
                echo '<script type="text/javascript"> window.location = "index.php#header" </script>';
        } catch (PDOException $e) {
                echo 'Erreur : ' . $e->getMessage();
        }
}
